package br.ufg.pos.fswm.foo.lista2.desafio;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/04/17.
 */
public class FibonacciTest {

    private Fibonacci sut;

    @Before
    public void setUp() throws Exception {
        sut = new Fibonacci();
    }

    @Test
    public void deve_devolver_o_primeiro_elemento_da_sequencia_fribonacci() throws Exception {
        assertEquals(1, sut.calculaFibonacci(1));
    }

    @Test
    public void deve_devolver_o_segundo_elemento_da_sequencia_fribonacci() throws Exception {
        assertEquals(1, sut.calculaFibonacci(2));
    }

    @Test
    public void deve_devolver_o_terceiro_elemento_da_sequencia_fribonacci() throws Exception {
        assertEquals(2, sut.calculaFibonacci(3));
    }

    @Test
    public void deve_devolver_o_quarto_elemento_da_sequencia_fribonacci() throws Exception {
        assertEquals(3, sut.calculaFibonacci(4));
    }

    @Test
    public void deve_devolver_o_quinto_elemento_da_sequencia_fribonacci() throws Exception {
        assertEquals(5, sut.calculaFibonacci(5));
    }

    @Test
    public void deve_devolver_o_sexto_elemento_da_sequencia_fribonacci() throws Exception {
        assertEquals(8, sut.calculaFibonacci(6));
    }
}
