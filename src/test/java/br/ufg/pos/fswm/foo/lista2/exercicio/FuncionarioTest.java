package br.ufg.pos.fswm.foo.lista2.exercicio;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/04/17.
 */
public class FuncionarioTest {

    private Funcionario sut;

    @Before
    public void setUp() throws Exception {
        sut = new Funcionario();
        sut.setNome("Bruno");
        sut.setDepartamento("TI");
        sut.setRg("123456789");
        sut.setSalario(1500.0);

        Data dataEntradaBanco = new Data();
        dataEntradaBanco.setDia(10);
        dataEntradaBanco.setMes(1);
        dataEntradaBanco.setAno(2017);

        sut.setDataEntradaBanco(dataEntradaBanco);
    }

    @Test
    public void deve_ser_possivel_aumentar_o_salario_do_funcionario() throws Exception {
        sut.receberAumento(500.0);

        assertEquals(2000.0, sut.getSalario(), 0.0001);
    }

    @Test
    public void deve_ser_possivel_calcular_ganho_anual_do_funcionario() throws Exception {
        final double salarioAnualCalculado = sut.calcularGanhoAnual();
        final double salarioAnualEsperado = 18000.0;

        assertEquals(salarioAnualEsperado, salarioAnualCalculado, 0.0001);
    }

    @Test
    public void deve_ser_possivel_visualizar_todos_dados_do_funcionario() throws Exception {
        sut.mostra();
    }

    @Test
    public void deve_comparar_dois_funcionarios_por_referencia() throws Exception {
        Funcionario f1 = new Funcionario();
        f1.setNome("Danilo");
        f1.setSalario(100);

        Funcionario f2 = new Funcionario();
        f1.setNome("Danilo");
        f1.setSalario(100);

        if (f1==f2) {
            System.out.println("Iguais");
            fail();
        } else {
            System.out.println("Diferentes");
        }
    }

    @Test
    public void deve_comparar_dois_funcionarios_na_mesma_referencia() throws Exception {
        Funcionario f1 = new Funcionario();
        f1.setNome("Hugo");
        f1.setSalario(100);

        Funcionario f2 = f1;

        if (f1==f2) {
            System.out.println("Iguais");
        } else {
            System.out.println("Diferentes");
            fail();
        }

    }
}
