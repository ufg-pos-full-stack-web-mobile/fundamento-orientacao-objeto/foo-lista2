package br.ufg.pos.fswm.foo.lista2.fixandoConhecimento;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/04/17.
 */
public class Porta {

    private boolean aberta;
    private String cor;
    private double dimensaoX;
    private double dimensaoY;
    private double dimensaoZ;

    void abre() {
        aberta = true;
    }

    void fecha() {
        aberta = false;
    }

    void pinta(String cor) {
        this.cor = cor;
    }

    boolean estaAberta() {
        return aberta;
    }

    public static void main(String... args) {
        Porta porta = new Porta();
        porta.pinta("vermelha");

        System.out.println("A cor da porta é " + porta.cor);

        porta.dimensaoX = 1;
        porta.dimensaoY = 5;
        porta.dimensaoZ = 0.1;

        System.out.println("Dimensoes: Largura = " + porta.dimensaoX + "\nAltura = " + porta.dimensaoY + "\nExpessura = " + porta.dimensaoZ);

        porta.abre();

        System.out.println("Porta aberta? " + porta.estaAberta());

        porta.fecha();

        System.out.println("Porta aberta? " + porta.estaAberta());
    }
}
