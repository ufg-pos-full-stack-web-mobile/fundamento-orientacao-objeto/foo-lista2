package br.ufg.pos.fswm.foo.lista2.fixandoConhecimento;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/04/17.
 */
public class Pessoa {

    private String nome;
    private int idade;

    public void fazAniversario() {
        this.idade++;
    }

    public static void main(String... args) {
        Pessoa pessoa = new Pessoa();
        pessoa.nome = "Bruno";
        pessoa.idade = 0;

        pessoa.fazAniversario();
        pessoa.fazAniversario();
        pessoa.fazAniversario();

        System.out.println("Idade depois de 3 aniversários: " + pessoa.idade);
    }
}
