package br.ufg.pos.fswm.foo.lista2.desafio;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/04/17.
 */
public class Fibonacci {
    public int calculaFibonacci(int elemento) {
        return elemento > 2 ? calculaFibonacci(elemento - 1) + calculaFibonacci(elemento - 2) : 1;
    }
}
