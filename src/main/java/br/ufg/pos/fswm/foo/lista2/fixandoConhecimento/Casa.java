package br.ufg.pos.fswm.foo.lista2.fixandoConhecimento;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/04/17.
 */
public class Casa {

    private String cor;
    private Porta porta1;
    private Porta porta2;
    private Porta porta3;

    void pinta(String cor) {
        this.cor = cor;
    }

    int quantasPortasEstaoAbertas() {
        int portasAberta = 0;
        if(porta1.estaAberta()) {
            portasAberta++;
        }
        if(porta2.estaAberta()) {
            portasAberta++;
        }
        if(porta3.estaAberta()) {
            portasAberta++;
        }
        return portasAberta;
    }

    public static void main(String... args) {
        Casa casa = new Casa();
        casa.pinta("Azul");

        System.out.println("A cor da casa é " + casa.cor);

        casa.porta1 = new Porta();
        casa.porta2 = new Porta();
        casa.porta3 = new Porta();

        casa.porta1.fecha();
        casa.porta2.abre();
        casa.porta3.abre();

        System.out.println("Tem " + casa.quantasPortasEstaoAbertas() + " porta(s) aberta(s) na casa");

        casa.porta2.fecha();
        casa.porta3.fecha();

        System.out.println("Agora tem " + casa.quantasPortasEstaoAbertas() + " porta(s) aberta(s) na casa");

    }
}
