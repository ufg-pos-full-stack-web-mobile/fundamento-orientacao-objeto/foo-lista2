package br.ufg.pos.fswm.foo.lista2.exercicio;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 15/04/17.
 */
public class Funcionario {

    private String nome;
    private String departamento;
    private double salario;
    private Data dataEntradaBanco;
    private String rg;

    public void receberAumento(double valorAumento) {
        this.salario += valorAumento;
    }

    public double calcularGanhoAnual() {
        return this.salario * 12;
    }

    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getDepartamento() {
        return departamento;
    }
    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }
    public double getSalario() {
        return salario;
    }
    public void setSalario(double salario) {
        this.salario = salario;
    }
    public String getRg() {
        return rg;
    }
    public void setRg(String rg) {
        this.rg = rg;
    }
    public Data getDataEntradaBanco() {
        return dataEntradaBanco;
    }
    public void setDataEntradaBanco(Data dataEntradaBanco) {
        this.dataEntradaBanco = dataEntradaBanco;
    }

    public void mostra() {
        System.out.println("Funcionario{" +
                "nome='" + nome + '\'' +
                ", departamento='" + departamento + '\'' +
                ", salario=" + salario +
                ", dataEntradaBanco='" + dataEntradaBanco + '\'' +
                ", rg='" + rg + '\'' +
                '}');
    }

}
