package br.ufg.pos.fswm.foo.lista2.desafio;

/**
 * @author Bruno Nogueira de Oliveira
 * @date 20/04/17.
 */
public class Main {

    public static void main(String... args) {
        final Fibonacci fibonacci = new Fibonacci();
        for(int i = 1 ; i <= 6 ; i++) {
            final int resultado = fibonacci.calculaFibonacci(i);
            System.out.println(resultado);
        }

    }
}
